package dev.refast.springlesson.sender;

import dev.refast.springlesson.order.Order;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class PonyExpressOrderSender implements OrderSender {

    @Override
    public void send(Order order) {
        System.out.println(String.format("Заказ №%s отправлен Пони Экспресс", order.getNumber()));
    }

    @Override
    public Boolean supports(Order order) {
        return Boolean.TRUE.equals(order.getIsVip());
    }
}
