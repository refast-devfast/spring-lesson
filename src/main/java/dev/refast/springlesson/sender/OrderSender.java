package dev.refast.springlesson.sender;

import dev.refast.springlesson.order.Order;

public interface OrderSender {
    void send(Order order);

    Boolean supports(Order order);
}
