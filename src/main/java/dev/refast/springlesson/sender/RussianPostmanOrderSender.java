package dev.refast.springlesson.sender;

import dev.refast.springlesson.order.Order;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("local")
public class RussianPostmanOrderSender implements OrderSender {
    @Override
    public void send(Order order) {
        System.out.println(String.format("Заказ №%s отправлен почтой Росии", order.getNumber()));
    }

    @Override
    public Boolean supports(Order order) {
        return Boolean.FALSE.equals(order.getIsVip());
    }
}
