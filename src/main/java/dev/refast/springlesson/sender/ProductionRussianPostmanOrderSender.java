package dev.refast.springlesson.sender;

import dev.refast.springlesson.order.Order;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile("production")
public class ProductionRussianPostmanOrderSender extends RussianPostmanOrderSender implements OrderSender {
    @Override
    public void send(Order order) {
        System.out.println(String.format("Реально все отправил почтой росии по заказу %s", order.getNumber()));
    }
}
