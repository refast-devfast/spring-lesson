package dev.refast.springlesson.configuration;

import dev.refast.springlesson.order.Order;
import dev.refast.springlesson.validator.BadOrderValidator;
import dev.refast.springlesson.validator.OrderValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.*;

import java.util.UUID;

@RequiredArgsConstructor
@Configuration
@EnableConfigurationProperties(OrderConfigurationProperty.class)
public class MainConfiguration {
    private final OrderConfigurationProperty orderConfigurationProperty;

    @Bean
    @Scope("prototype")
    public Order orderTest() {
        var orderUuid = UUID.randomUUID();
        var orderPrefix = orderConfigurationProperty.getPrefix();
        var orderNumber = String.format("%s_номер_%s", orderPrefix, orderUuid);

        var order = new Order();
        order.setNumber(orderNumber);
        order.setCatalogId(1);
        order.setIsVip(false);
        order.setExists(true);

        return order;
    }

    @ConditionalOnProperty(value = "springlesson.order.bad-validator", havingValue = "true", matchIfMissing = true)
    @Bean
    public OrderValidator badOrderValidator() {
        return new BadOrderValidator();
    }
}
