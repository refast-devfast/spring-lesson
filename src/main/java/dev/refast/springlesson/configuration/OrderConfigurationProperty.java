package dev.refast.springlesson.configuration;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "springlesson.order")
public class OrderConfigurationProperty {
    private String prefix;

    private String flow;
}
