package dev.refast.springlesson.order;

import lombok.Data;

@Data
public class Order {
    private String number;

    private Integer catalogId;

    private Boolean isVip;

    private Boolean exists;
}
