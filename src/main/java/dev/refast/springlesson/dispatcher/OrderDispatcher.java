package dev.refast.springlesson.dispatcher;

import dev.refast.springlesson.order.Order;
import dev.refast.springlesson.sender.OrderSender;
import dev.refast.springlesson.validator.OrderValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class OrderDispatcher {
    private final List<OrderSender> orderSenders;

    private final List<OrderValidator> orderValidators;

    private final Order order;

    private final Order order2;

    public void dispatch() {
        System.out.println("Я запустился и диспатчу заказы");

        dispatchOrder(order);
        dispatchOrder(order2);
    }

    private void dispatchOrder(Order order) {
        for (var validator : orderValidators) {
            if (Boolean.FALSE.equals(validator.validate(order))) {
                throw new RuntimeException("Заказ невалидный!!!");
            }
        }

        for (var sender : orderSenders) {
            if (Boolean.TRUE.equals(sender.supports(order))) {
                sender.send(order);
                break;
            }
        }
    }
}
