package dev.refast.springlesson.validator;

import dev.refast.springlesson.order.Order;

public interface OrderValidator {
    Boolean validate(Order order);
}
