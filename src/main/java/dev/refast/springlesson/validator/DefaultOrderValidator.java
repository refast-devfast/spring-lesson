package dev.refast.springlesson.validator;

import dev.refast.springlesson.order.Order;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class DefaultOrderValidator implements OrderValidator {
    @Override
    public Boolean validate(Order order) {
        return Objects.nonNull(order.getCatalogId());
    }
}
