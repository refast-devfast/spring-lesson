package dev.refast.springlesson.validator;

import dev.refast.springlesson.order.Order;
import org.springframework.stereotype.Service;

@Service
public class ProductExistsOrderValidator implements OrderValidator {
    @Override
    public Boolean validate(Order order) {
        return Boolean.TRUE.equals(order.getExists());
    }
}
