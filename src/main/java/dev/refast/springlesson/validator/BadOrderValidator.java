package dev.refast.springlesson.validator;

import dev.refast.springlesson.order.Order;

public class BadOrderValidator implements OrderValidator {
    @Override
    public Boolean validate(Order order) {
        return false;
    }
}
