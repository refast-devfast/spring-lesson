package dev.refast.springlesson;

import dev.refast.springlesson.dispatcher.OrderDispatcher;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        var context = SpringApplication.run(Application.class, args);
        var dispatcher = context.getBean(OrderDispatcher.class);
        dispatcher.dispatch();
    }
}
